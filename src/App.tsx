import React from 'react';
import './App.css';
import {
  Route,
  Link,
  Routes,
  BrowserRouter
} from "react-router-dom";
import Page1 from './Pages/Page1';
import Page2 from './Pages/Page2';
import Home from './Pages/Home';
import Login from './Pages/Login';
import { NavBar } from "./components/NavBar";

function App() {
const css = {
  padding: '10px',
  font: 'Aryal',
  margin:'50px',
  size: '24px',
};  return (
    <div className="App">
        <BrowserRouter>
        <NavBar />

        <Routes>
          <Route index element={<Home />} />
          <Route path="login" element={<Login />} />
          <Route path="page1" element={<Page1 />} />
          <Route path="page2" element={<Page2 />} />        
          <Route path="*" element={<span className='classmenu'>Страница не найдена</span>} />
        </Routes>
        

      </BrowserRouter >
    
    </div>
   
  );
}

export default App;
