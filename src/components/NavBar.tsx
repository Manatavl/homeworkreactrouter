import React from 'react';
import { Navbar, Container, Nav } from 'react-bootstrap';
import {Link} from "react-router-dom";
import '../App.css';

export const NavBar = () => {
    const css = {
        padding: '10px',
        font: 'Aryal',
        margin:'20px',
    };
    return (
        
<Navbar sticky="top" expand="lg" className='classmenu'>
    <Container>
        <Navbar.Collapse id="responsive-navbar-nav">
            <Nav>
                <Link to="/Home" style={css}></Link>
                <Link to="/login" style={css}>Вход</Link>
                <Link to="/page1" style={css}>страница 1</Link>
                <Link to="/page2" style={css}>страница 2</Link>
            </Nav>
        </Navbar.Collapse>
    </Container>
</Navbar>
    );
};