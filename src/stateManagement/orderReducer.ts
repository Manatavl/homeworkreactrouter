import { OrderThing } from "../models/orderThing";

interface State {
    list: OrderThing[];
}

// Исходный стейт
const initialState: State = {
    list: [],
}

// редюсер - функция, на основе входного объекта 
// меняющая стейт и возвращающая новый
const orderReducer = (state = initialState, action: {type:string, payload?:any}) => {
    switch (action.type) {


        case 'FANCY_ORDER_CHECK':
            const list = [...state.list];
            const item = action.payload;
            const index = list.findIndex(x => x.id === item.id);
            if (index === -1) {
                list.push(item);
            } else {
                list.splice(index, 1);
            }
            const newState = { ...state, list: [...list] };
            return newState;

        case '[ORDER_STATE] ORDER_CLEAR':
            return { ...state, list: [] };
        default:
            return state;
    }
};
export default orderReducer;