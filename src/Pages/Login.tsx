import { useState, useEffect } from 'react';
import './Login.css';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';

function sendLogin(email:  string, passw: string) {
    console.log('email=' + email);
    console.log('passw=' + passw);
    fetch('https://jsonplaceholder.typicode.com/posts');
    
    window.location.assign('/'); 
  }

function Login() {
    const [email, setemail] = useState('');
    const [passw, setpassw] = useState('');
    const css = {
      margin: '50px',      
  };
  return (
    <div className="block">
      
        <TextField name="email" id="email" label="email" variant="standard" 
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            console.log('onChangeEmail'); setemail(event.target.value);}}
        />
     
        <TextField name="passw" id="passw" label="Пароль" variant="standard" type='password'
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              console.log('onChangepassw'); setpassw(event.target.value);}}
        />
    
      <div>
        <Button sx={{ margin: '25px' }} color='info' size='medium' variant="text" className='css'
        onClick={() => sendLogin(email, passw)}>Вход</Button>
      </div>
    </div>
  );
}

export default Login;
